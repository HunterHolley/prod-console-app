# Demo Product Application

## Instructions to run the application.
1. Right-click on Solution
1. Click on the "Restore NuGet Packages" option
1. Open Package Manager Console
	1. Click "View" menu option
	1. Select "Other Windows" option
	1. and finally select "Package Manager Console"
1. Run command "Update-Database"
1. Press "F5" to start the application.
1. Enjoy!!!