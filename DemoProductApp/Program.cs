﻿using System;

namespace DemoProductApp
{
    class Program
    {
        static void Main()
        {
            DatabaseContext db = new DatabaseContext();

            while (true)
            {
                try
                {
                    var product = new Product();

                    Console.WriteLine("Enter a new product:\n");

                    Console.Write("Enter product name: ");
                    product.Name = Console.ReadLine();

                    Console.Write("Enter product description: ");
                    product.Description = Console.ReadLine();

                    Console.Write("Enter product SKU: ");
                    product.SKU = Console.ReadLine();

                    Console.Write("Enter product price: ");
                    product.Price = Convert.ToDecimal(Console.ReadLine());

                    Console.WriteLine("Inserting new product...");

                    db.Products.Add(product);
                    db.SaveChanges();

                    Console.WriteLine("Created product successfully.");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"An error has occurred creating the product.\n Message: { e.Message}\n");
                }

                string answer;
                bool notValid;

                do
                {
                    Console.Write("Do you want to enter another product? (Y/N): ");

                    answer = Console.ReadLine();
                    notValid = answer.ToLower() != "y" && answer.ToLower() != "n";

                    if (notValid)
                    {
                        Console.WriteLine("Invalid entry. Please try again...\n");
                    }
                } while (notValid);


                if (answer.ToLower() == "n")
                {
                    break;
                }
                Console.WriteLine();
            }
        }
    }

}