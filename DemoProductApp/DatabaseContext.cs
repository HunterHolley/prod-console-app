﻿using System.Data.Entity;

namespace DemoProductApp
{

    class DatabaseContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
    }

}